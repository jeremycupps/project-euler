/*** Define inputs ***/

const maxValue = 4000000;


/*** Define functions ***/

// Generates a Fibonacci sequence as an array up to at the specified
// max value
const fibonate = (max = 2, values = [1, 2]) => {
  const next = values[(values.length - 1)] + values[(values.length - 2)];
  return next <= max ? fibonate(max, [...values, next]) : values;
}

// Function that filters an array for even values
const evens = numbers => numbers.filter(n => !(n % 2));

// Function that computes the sum of all of the numbers in the <numbers> array
const sum = numbers => numbers.reduce((acc, val) => acc + val, 0);


/*** Solution ***/

const result = sum(evens(fibonate(maxValue)));

console.log(result);
