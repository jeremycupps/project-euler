/*** Define inputs ***/

const upperLimit = 100;


/*** Define functions and consts ***/

const numbers = Array.from(new Array(upperLimit), (_, i) => i + 1);

const sumSquares = (nums) => nums.reduce((a, c) => a + c * c);

const squareSums = (nums) => {
  const sum = nums.reduce((a, c) => a + c);
  return sum * sum;
}

const sumSquareDiff = (nums) => squareSums(nums) - sumSquares(nums);


/*** Solution ***/

const answer = sumSquareDiff(numbers);

console.log(answer);