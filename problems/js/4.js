/*** Define inputs ***/

const factorLength = 3;


/*** Define functions ***/

const reverse = (str) => [...str].reduce((acc, cur, i) => [cur, ...acc], []).join('');

const isPalindrome = (num) => {
  const numString = String(num);
  const halfLength = Math.ceil(numString.length / 2);
  const isEven = numString.length % 2 === 0;
  const firstHalf = numString.substring(0,halfLength);
  // Handle both even and odd-length numbers.
  // For odd-length, the middle character is included in both the first and second halves.
  const secondHalf = numString.substring(isEven ? halfLength : (halfLength - 1));
  return firstHalf === reverse(secondHalf);
}

/**
 * Find the largest product of two numbers of the specified factorLength
 * that meets the given condition
 * @param {Number} factorLength 
 * @param {Function} condition 
 */
const findLargestProduct = (factorLength, condition) => {
  const seed = Math.pow(10, factorLength) - 1;
  const floor = Math.pow(10, factorLength - 1);

  let x = seed,
    y = seed;

  let lastX = x,
    lastY = y,
    decrementX = false,
    result = null;

  // The following algorithm was the most efficient I could come up with
  // to calculate all of the products of a series of numbers from largest
  // product to smallest descending.
  while(!result && x >= floor && y >= floor) {
    while(x <= seed) {
      const next = x++ * y--;

      if (condition(next)) {
        result = next;
        break;
      }
    }

    if (x > seed) {
      if (decrementX) lastX -= 1;
      else lastY -= 1;

      x = lastX;
      y = lastY;

      decrementX = !decrementX;
    }
  }

  return result;
}

/*** Solution ***/

const answer = findLargestProduct(factorLength, isPalindrome);

console.log(answer);