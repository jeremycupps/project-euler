/*** Define inputs ***/

const number = 600851475143;


/*** Define functions ***/

const max = nums => nums.reduce((acc, cur) => cur > acc ? cur : acc);

// Returns non unique prime factors of <num>
// (may have repeating values)
const primeFactorsOf = (num) => {
  const result = [];
  let n = num;

  if (n % 2 === 0) result.push(2);

  let f = 3;
	while (n > 1) {
		if (n % f === 0) {
			result.push(f);
      n = n / f;
    } else f += 2;
  }

	return result;
}


/*** Solution ***/

const result = max(primeFactorsOf(number));

console.log(result);
