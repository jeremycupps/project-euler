/*** Define inputs ***/

const upperBound = 999;
const multiplesOf = [3, 5];


/*** Define functions ***/

// Curried function accepting first a number to test multiples of
// and then a value to perform the test against, returning the result
// of the test.
// E.g.:
//    isMultipleOf(3)(9) => true
//    isMultipleOf(3)(10) => false
const isMultipleOf = number => value => value % number === 0;

// Function that computes the sum of all of the numbers in the <numbers> array
const sum = numbers => numbers.reduce((acc, val) => acc + val, 0);


/*** Solution ***/

// Create an array of all numbers sequentially from 1 through the upper bound
const numbers = Array.from(new Array(upperBound), (_, i) => i + 1);

// Array of test functions for each of the specified multiplesOf numbers
const isMultipleTests = multiplesOf.map(isMultipleOf);

// Filtered array of all the numbers that are multiples of the specified numbers
const multiples = numbers.filter(n => isMultipleTests.find(t => t(n)));

const result = sum(multiples);

console.log(result);
