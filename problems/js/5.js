/*** Define inputs ***/

const upperLimit = 20;


/*** Define functions and consts ***/

// Create an array of all numbers from 1 up through the upper limit
const nums = Array.from(new Array(upperLimit), (_, i) => i + 1);

/**
 * Calculates the greatest common divisor of two numbers based on Euclide's algorithm
 */
function gcd(a, b) {
  let x = a,
    y = b;

  while (x !== y) {
    if (y > x) y = y - x;
    else x = x - y;

    if (x === 1 || y === 1) return 1;
  }

  return x;
}

/**
 * Calculates the least common multiple of all the number arguments
 */
function lcm(...args) {
  return args.reduce((r, v) => (r * v) / gcd(r, v));
}


/*** Solution ***/

const answer = lcm(...nums);

console.log(answer);