# Project Euler

Problems: https://projecteuler.net/archives

Numeric solutions: https://github.com/luckytoilet/projecteuler-solutions/blob/master/Solutions.md